package at.campus02.unoKyk.ke;

import java.util.ArrayList;

public class Player {

	private String name;
	protected ArrayList<Card> handcards;
	
	public Player(String name) {
		this.name = name;
		handcards = new ArrayList<>();
	}

	public String getName() {
		return name;
	}
	
	public ArrayList<Card> getHandcards() {
		return handcards;
	}
	
	public void takeCard(Card c) {
		handcards.add(c);
	}
	
	

	public void done() {
		if (handcards.isEmpty()) {
			System.out.printf("%s: Uno Uno !", name);
		}
	}
	
	public void unoCard() {
		if (handcards.size() == 1) {
			
		}
	}
	
	public String toString() {
	
		return String.format("%s", name);
	}
		

	
	
}
