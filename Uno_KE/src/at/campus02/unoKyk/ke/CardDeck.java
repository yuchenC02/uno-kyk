package at.campus02.unoKyk.ke;

import java.util.ArrayList;
import java.util.Collections;

public class CardDeck {

	ArrayList<Card> deck;
	private ArrayList<Card> discardPile;
//	private ArrayList<Card> handcards;
	
	public CardDeck() {
		deck = new ArrayList<Card>();
		
		for (int cardvalue=0; cardvalue <=8; cardvalue++) {
			deck.add(new Card(cardvalue, "yellow", cardvalue));
			deck.add(new Card(cardvalue, "green", cardvalue));
			deck.add(new Card(cardvalue, "blue", cardvalue));
			deck.add(new Card(cardvalue, "red", cardvalue));
		}
		
		for (int cardvalue=1; cardvalue <=8; cardvalue++) {
			deck.add(new Card(cardvalue, "yellow", cardvalue));
			deck.add(new Card(cardvalue, "green", cardvalue));
			deck.add(new Card(cardvalue, "blue", cardvalue));
			deck.add(new Card(cardvalue, "red", cardvalue));
		}
		
		for (int i=0; i <= 1; i++) {				// 10 = skip-card
			deck.add(new Card(10, "yellow", 20));  
			deck.add(new Card(10, "green", 20));  
			deck.add(new Card(10, "blue", 20));  
			deck.add(new Card(10, "red", 20));  
		}

		for (int i=0; i <= 1; i++) {				// 11 = reverse-card
			deck.add(new Card(11, "yellow", 20));  
			deck.add(new Card(11, "green", 20));  
			deck.add(new Card(11, "blue", 20));  
			deck.add(new Card(11, "red", 20));  
		}

		for (int i=0; i <= 1; i++) {				// 12 = draw2-card
			deck.add(new Card(12, "yellow", 20));  
			deck.add(new Card(12, "green", 20));  
			deck.add(new Card(12, "blue", 20));  
			deck.add(new Card(12, "red", 20));  
		}
		
		for (int i=0; i <= 3; i++) {
			deck.add(new Card(13, "black", 50));	// 13 = wild-card
			deck.add(new Card(14, "black", 50));	// 14 = draw4-card
		}
	}
		
	
	public ArrayList<Card> getCards() {
		return deck;
	}
	
	public void shuffleDeck() {
		Collections.shuffle(deck);
		Collections.shuffle(deck);
		Collections.shuffle(deck);
	}
	

	
	
	
}
