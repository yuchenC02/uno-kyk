package at.campus02.unoKyk.ke;

public class Card {

	private int cardNumber;
	private String cardColor;
	private int cardValue;
	
	public Card(int cardNumber, String cardColor, int cardValue) {
		this.cardNumber = cardNumber;
		this.cardColor = cardColor;
		this.cardValue = cardValue;
	}
	
	public int getCardNumber() {
		return cardNumber;
	}
	
	public String getCardColor() {
		return cardColor;
	}
	
	public int getCardValue() {
		return cardValue;
	}
	
//	public String toString() {
//		String cardNum = null;
//		if (cardNumber <= 9) {
//			cardNum = Integer.toString(cardNumber);
//		}
//	}
	
	
	
}
