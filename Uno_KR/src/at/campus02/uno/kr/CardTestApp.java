
package at.campus02.uno.kr;
import java.util.Scanner;

public class CardTestApp {

	public static void main(String[] args) {
		
		//matchCard-Test:
		
		Card card1 = new Card(2,"red",2,"null");
		Card card2 = new Card(8, "red", 8, "null");
		Card card3 = new Card(13,"black",50,"wildFour");
		Card card4 = new Card(10, "yello", 20, "reverse");
		Card card5 = new Card(8,"blue",8,"null");
		Card card6 = new Card(14,"black",50,"changeColor");
		
		System.out.println(card1);
		
		System.out.println(card1.matchCard(card2));
		System.out.println(card1.matchCard(card3));
		System.out.println(card1.matchCard(card4));
		System.out.println(card1.matchCard(card5));
		System.out.println(card2.matchCard(card3));
		System.out.println(card3.matchCard(card4));
	
		
		//colorChange-Test:
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println(card6);
		
		if (card6.getCardNum() == 14) {
			System.out.println("Bitte w�hlen Sie eine neue Farbe:");
		}

		String color = scanner.nextLine();

		System.out.println("Sie haben die Farbe '" + color + "' gew�hlt.");

		scanner.close();
	
		card6.setCardColor(color);
		System.out.println(card6);
		}
}

