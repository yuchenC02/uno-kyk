package at.campus02.uno.kr;

import java.util.Scanner;

public class UnoDemoApp {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		Unoplay play1 = new Unoplay();
		System.out.println(play1);
		
//		play1.addPlayer(new Player("Kathrin"));
//		play1.addPlayer(new Player("Yu"));
//		play1.addPlayer(new Player("Katharina"));
		
		System.out.println("How many players?");
		int anzahl = scanner.nextInt();
		System.out.println(anzahl + " players joining the game.");
		
		for (int i = 1; i <= anzahl; i++) {
			System.out.println("Please enter your username: ");
			String user = scanner.next();
			Player player = new Player(user);
		
			play1.addPlayer(player);
		}
		
		play1.shuffle();
		
		play1.giveCard();
		System.out.println(play1);
		
		//while(play1.turn());
		//System.out.println(play1.turn());
		
		System.out.println("Who first?");
		String name = scanner.next();
		System.out.println(name + " begins.");

		scanner.close();	
	}
	
	
	}

