package at.campus02.uno.kr;

import java.util.ArrayList;

public class Player extends Unoplay{

	private String name;
	private ArrayList<Card> handcards = new ArrayList<Card>();

	public Player(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public ArrayList<Card> getHandcards() {
		return handcards;
	}

	public void takeCard(Card a) {
		handcards.add(a);
	}

	public Card chooseCard(Card cardOnStack) {  // wenn f�r eine Handkarte die Methode
		for (Card card : handcards) {           // matchCard() "true" zur�ckliefert, wird
			if (card.matchCard(cardOnStack)) {  // die Karte aus den Handkarten entfernt
												// und der Wert der Karte zur�ckgegeben
				handcards.remove(card);

				return card;
			}
		}
		return null;
	}

	public boolean unoUno() {
		return handcards.isEmpty();

	}
	
//	public String toString() {
//		return String.format("%s hat %d Karten", name, handcards.size());
//	}
	
	public String toString() {
		StringBuilder b = new StringBuilder();
		for (Card card : handcards) {
			b.append(card.toString());
		}
		return String.format("%s has %d cards: %s \n", name, handcards.size(), b.toString());
	}
	
	
}
