package at.campus02.uno.kr;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;



public class Unoplay {

	private ArrayList<Card> takeCardStack = new ArrayList<Card>();
	private ArrayList<Card> dropCardStack = new ArrayList<Card>();
	private ArrayList<Player> players = new ArrayList<Player>();

	public Unoplay() {
		for (int kartenwert = 0; kartenwert <= 9; kartenwert++) {
			takeCardStack.add(new Card(kartenwert, "green", kartenwert, "null"));
			takeCardStack.add(new Card(kartenwert, "red", kartenwert, "null"));
			takeCardStack.add(new Card(kartenwert, "blue", kartenwert, "null"));
			takeCardStack.add(new Card(kartenwert, "yellow", kartenwert, "null"));	
		}
		
		for (int kartenwert = 1; kartenwert <= 9; kartenwert++) {
			takeCardStack.add(new Card(kartenwert, "green", kartenwert, "null"));
			takeCardStack.add(new Card(kartenwert, "red", kartenwert, "null"));
			takeCardStack.add(new Card(kartenwert, "blue", kartenwert, "null"));
			takeCardStack.add(new Card(kartenwert, "yellow", kartenwert, "null"));	
		}
		
		for (int i = 1; i <= 2; i++) {
			takeCardStack.add(new Card(10, "green", 20, "reverse"));
			takeCardStack.add(new Card(10, "red", 20, "reverse"));
			takeCardStack.add(new Card(10, "blue", 20, "reverse"));
			takeCardStack.add(new Card(10, "yellow", 20, "reverse"));
		}
		
		for (int i = 1; i <= 2; i++) {
			takeCardStack.add(new Card(11, "green", 20, "takeTwo"));
			takeCardStack.add(new Card(11, "red", 20, "takeTwo"));
			takeCardStack.add(new Card(11, "blue", 20, "takeTwo"));
			takeCardStack.add(new Card(11, "yellow", 20, "takeTwo"));
		}
		
		for (int i = 1; i <= 2; i++) {
			takeCardStack.add(new Card(12, "green", 20, "blocked"));
			takeCardStack.add(new Card(12, "red", 20, "blocked"));
			takeCardStack.add(new Card(12, "blue", 20, "blocked"));
			takeCardStack.add(new Card(12, "yellow", 20, "blocked"));
		}
		
		for (int i = 1; i <= 4; i++) {
			takeCardStack.add(new Card(13, "black", 50, "changeColor"));
			takeCardStack.add(new Card(14, "black", 50, "wildFour"));
		}

	}

	public void shuffle() { // mischt den Abhebestapel
		Collections.shuffle(takeCardStack);
	}

	public void addPlayer(Player newPlayer) { // f�gt einen Spieler hinzu
		players.add(newPlayer);
	}

	public Player getNext(Player thisPlayer) {
		int temp;

		for (Player player : players) {
			if (player.equals(thisPlayer)) {
				temp = players.indexOf(thisPlayer);
				temp += 1;
				return players.get(temp);
			}
		}

		return null;
	}

	public void giveCard() {
		System.out.println("...dealing out cards ...");
		for (int counter = 0; counter < 7; counter++) { // counter f�r Handkartenanzahl
			for (Player player : players) {
				Card givenCard = takeCardStack.remove(0); // neuem Card-Objekt wird der Wert der
															// obersten Karte am Stapel zugewiesen
				player.takeCard(givenCard); // mit der Methode takeCard wird die Karte
			} // in die Handkarten aufgenommen
		}
	}

	public void takeTwo(Player thisPlayer) {

		Card c = dropCardStack.remove(0);
		int thisCardNo = c.getCardNum();

		if (thisCardNo == 10) { // takeTwo: 10; blocked: 11; reverse: 12;

			Player p = getNext(thisPlayer); // Wenn ja, wird der n�chste Spieler mittels getNext Methode aufgerufen
			Card takeFirst = takeCardStack.remove(0); // ein Kartenobjekt erstellt und der Wert der ersten Karte am
														// Abhebestapel zugwiesen
			p.takeCard(takeFirst); // dem Spieler zugewiesen
			Card takeSec = takeCardStack.remove(0); // erneut ein Kartenobjekt erstellt
			p.takeCard(takeSec); // dem Spieler zugewiesen
		}
	}
	// der n�chste Spieler hat zwei Karten erhalten

	public Player blocked(Player thisPlayer) {

		Card c = dropCardStack.remove(0); // der Karte c wird der Wert der obersten Karte am Ablegestapel zugewiesen
		int thisCardNo = c.getCardNum(); // der Kartenwert dieser Karte wird ausgelesen

		if (thisCardNo == 11) { // ist der Kartenwert 11, handelt es sich um eine gesperrt-Karte
			Player blocked = getNext(thisPlayer);
			Player next = getNext(blocked);

			return next;
		}
		return null;
	}
	
	public Card changeColor(Card thisCard) {
		
	Scanner scanner = new Scanner(System.in);
		
	System.out.println(thisCard);
		
	if (thisCard.getCardNum() == 14) {
		System.out.println("Bitte w�hlen Sie eine neue Farbe:");
		

		String color = scanner.nextLine();

		System.out.println("Sie haben die Farbe '" + color + "' gew�hlt.");

		scanner.close();
	
		thisCard.setCardColor(color);
		System.out.println(thisCard);
		}
	return thisCard;
	}
	
	
	public ArrayList<Player> reverse() {

		Card c = dropCardStack.remove(0);
		int thisCardNo = c.getCardNum();

		if (thisCardNo == 12) {
			Player p1 = players.get(0);
			p1 = players.get(2);
			Player p2 = players.get(0);

			players.add(p1);
			players.add(p2);
		}
		return players;
	}

	public boolean turn() { // Spielzug eines Spielers:

		Player thisPlayer = players.remove(0);
		System.out.printf("%s: ", thisPlayer.getName());

		Card thisCard = dropCardStack.get(0);
		Card handcard = thisPlayer.chooseCard(thisCard);

		if (handcard != null) {    //wenn eine passende Karte in den Handkarten vorhanden
			dropCardStack.add(0, handcard);
			System.out.printf("legt %s ab \n", handcard);
		} else {                  
			System.out.println("muss ziehen, ");
			Card newCard = takeCardStack.remove(0);

			thisPlayer.getHandcards().add(newCard);
			
//
//			if (newCard.matchCard(thisCard)) {
//				dropCardStack.add(0, newCard);
//				System.out.printf(" legt %s ab \n", newCard);
//			} else {
//				thisPlayer.getHandcards().add(newCard);
//				System.out.printf("%s muss Karte aufnehmen \n", thisPlayer.getName());
//			}
		}
			
		players.add(thisPlayer);
		
		return players.size() > 1;

	}
	// damit im n�chsten Spielzug der n�chste Spieler im
	// Array dran ist, wird der aktuelle Spieler aus dem
	// Array entfernt und am Ende des Spielzuges wieder
	// eingef�gt
	// eine Karte wird vom Abhebestapel genommen
	// die Handkarte wird der Karte am Ablegestapel verglichen
	// eine passende Handkarte wird abgelegt
	// wenn es sich um eine Aktionskarte handelt, wird die
	// Aktion ausgef�hrt
	// passt keine Handkarte, wird eine Karte vom Abhebestapel
	// aufgenommen
	// Spieler wird in Array hinzugef�gt
	// die Anzahl der Spieler wird zur�ckgegeben bis nur mehr
	// ein Spieler vorhanden ist oder ein Spieler > 500 Punkte hat

	private Card takeCard() {

		if (takeCardStack.isEmpty()) {
			System.out.println("\n --- Karten werden gemischt ---");
			Card thisCard = dropCardStack.remove(0);
			takeCardStack.addAll(dropCardStack);
			shuffle();

			dropCardStack.clear();
			dropCardStack.add(thisCard);

			return takeCardStack.remove(0);
		}

		else {
			Card takeCard = takeCardStack.remove(0);
			return takeCard;
		}
	}
	
	// ist der Abhebestapel leer, wird die oberste Karte vom
	// Ablegestapel entfernt und deren Wert gespeichert (Klasse Card)
	// die Karten vom Ablegestapel werden in den Abhebestapel hinzugef�gt
	// und gemischt
	// die gespeicherte Karte wird dem Ablegestapel hinzugef�gt
	// ist der Abhebestapel niciht leer, wird der Wert der abgehobenen Karte
	// zur�ckgegeben
	
	public String toString() {
		
		StringBuilder b = new StringBuilder();
		for (Player player : players) {
			b.append(player.toString());
		}
		
		if (dropCardStack.isEmpty()) {
			return String.format("%s%d cards are on the stack.", b.toString(),
					takeCardStack.size());
		}

		return String.format("%s %d cards are on the stack, %s", b.toString(),
				takeCardStack.size(), dropCardStack.get(0));
	}
}
