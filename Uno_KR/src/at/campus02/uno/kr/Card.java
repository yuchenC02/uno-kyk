package at.campus02.uno.kr;

public class Card  {

	private int cardNum;
	private String cardColor;
	private int cardPoint;
	private String cardAction;

	public Card(int cardNum, String cardColor, int cardPoint, String cardAction) {
		
		this.cardNum = cardNum;
		this.cardColor = cardColor;
		this.cardPoint = cardPoint;
		this.cardAction = cardAction;
	}
	
	
	public int getCardNum() {
		return cardNum;
	}

	public String getCardColor() {
		return cardColor;
	}

	public int getCardPoint() {
		return cardPoint;
	}

	public String getCardAction() {
		return cardAction;
	}

	public boolean matchCard(Card cardOnStack) { // pr�ft, ob die aktuelle Karte mit der am Stapel
		 
		// liegenden Karte in Zahl und Farbe �bereinstimmt
		
		if(this.cardColor == "black") {          //ist in den Handkarten eine schwarze Aktionskarte
		this.cardColor = cardOnStack.getCardColor();  //wird ihr die Farbe der Karte am Ablegestapel zugewiesen
		                                              //damit sie im weiteren Schritt gepr�ft und gespielt werden kann
		                                              //(scharze Aktionskarten k�nnen immer gespielt werden)
		}
		
//		if(cardOnStack.getCardColor() == "black")     //Pr�fen, welche Karten auf wildFour und ColorChange gespielt werden k�nnen
//			return true;
		
		if (this.cardColor == cardOnStack.getCardColor())        // gleiche Farbe?
			return true;
		if (this.cardNum == cardOnStack.getCardNum())            // gleiche Zahl?
			return true;
		

		return false;
	}

	/**
	 * @param cardColor the cardColor to set
	 */
	public void setCardColor(String cardColor) {
		this.cardColor = cardColor;
	}


	public String toString() {
		return String.format("[%d_%s]", cardNum, cardColor);
	}
}
