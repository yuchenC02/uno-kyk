import java.util.ArrayList;
import java.util.Collections;

import uno.Card;


public class CardHandler {
	public ArrayList<Cards> cards;
    public ArrayList<Cards> drawPile;
    public CardHandler() {
        cards= new ArrayList<>();
       
        cards.add(new Cards(0, "red","no action"));
        cards.add(new Cards(1, "red","no action"));
        cards.add(new Cards(1, "red","no action"));
        cards.add(new Cards(2, "red","no action"));
        cards.add(new Cards(2, "red","no action"));
        cards.add(new Cards(3, "red","no action"));
        cards.add(new Cards(3, "red","no action"));
        cards.add(new Cards(4, "red","no action"));
        cards.add(new Cards(4, "red","no action"));
        cards.add(new Cards(5, "red","no action"));
        cards.add(new Cards(5, "red","no action"));
        cards.add(new Cards(6, "red","no action"));
        cards.add(new Cards(6, "red","no action"));
        cards.add(new Cards(7, "red","no action"));
        cards.add(new Cards(7, "red","no action"));
        cards.add(new Cards(8, "red","no action"));
        cards.add(new Cards(8, "red","no action"));
        cards.add(new Cards(9, "red","no action"));
        cards.add(new Cards(9, "red","no action"));
        cards.add(new Cards(20, "red","skip")); //20 is skip
        cards.add(new Cards(20, "red","skip")); //20 is skip
        cards.add(new Cards(20, "red","2+")); //20 is draw two
        cards.add(new Cards(20, "red","2+")); //20 is draw two
        cards.add(new Cards(20, "red","reverse")); //20 is reverse
        cards.add(new Cards(20, "red","reverse")); //20 is reverse
        cards.add(new Cards(50, "any","wild")); //50 is wild
        cards.add(new Cards(50, "any","4+ wild")); //50 is draw 4 wild
        cards.add(new Cards(0, "blue","no action"));
        cards.add(new Cards(1, "blue","no action"));
        cards.add(new Cards(1, "blue","no action"));
        cards.add(new Cards(2, "blue","no action"));
        cards.add(new Cards(2, "blue","no action"));
        cards.add(new Cards(3, "blue","no action"));
        cards.add(new Cards(3, "blue","no action"));
        cards.add(new Cards(4, "blue","no action"));
        cards.add(new Cards(4, "blue","no action"));
        cards.add(new Cards(5, "blue","no action"));
        cards.add(new Cards(5, "blue","no action"));
        cards.add(new Cards(6, "blue","no action"));
        cards.add(new Cards(6, "blue","no action"));
        cards.add(new Cards(7, "blue","no action"));
        cards.add(new Cards(7, "blue","no action"));
        cards.add(new Cards(8, "blue","no action"));
        cards.add(new Cards(8, "blue","no action"));
        cards.add(new Cards(9, "blue","no action"));
        cards.add(new Cards(9, "blue","no action"));
        cards.add(new Cards(20, "blue","skip")); //20 is skip
        cards.add(new Cards(20, "blue","skip")); //20 is skip
        cards.add(new Cards(20, "blue","2+")); //20 is draw two
        cards.add(new Cards(20, "blue","2+")); //20 is draw two
        cards.add(new Cards(20, "blue","reverse")); //20 is reverse
        cards.add(new Cards(20, "blue","reverse")); //20 is reverse
        cards.add(new Cards(50, "any","wild")); //50 is wild
        cards.add(new Cards(50, "any","4+ wild")); //50 is draw 4 wild
        cards.add(new Cards(0, "green","no action"));
        cards.add(new Cards(1, "green","no action"));
        cards.add(new Cards(1, "green","no action"));
        cards.add(new Cards(2, "green","no action"));
        cards.add(new Cards(2, "green","no action"));
        cards.add(new Cards(3, "green","no action"));
        cards.add(new Cards(3, "green","no action"));
        cards.add(new Cards(4, "green","no action"));
        cards.add(new Cards(4, "green","no action"));
        cards.add(new Cards(5, "green","no action"));
        cards.add(new Cards(5, "green","no action"));
        cards.add(new Cards(6, "green","no action"));
        cards.add(new Cards(6, "green","no action"));
        cards.add(new Cards(7, "green","no action"));
        cards.add(new Cards(7, "green","no action"));
        cards.add(new Cards(8, "green","no action"));
        cards.add(new Cards(8, "green","no action"));
        cards.add(new Cards(9, "green","no action"));
        cards.add(new Cards(9, "green","no action"));
        cards.add(new Cards(20, "green","skip")); //20 is skip
        cards.add(new Cards(20, "green","skip")); //20 is skip
        cards.add(new Cards(20, "green","2+")); //20 is draw two
        cards.add(new Cards(20, "green","2+")); //20 is draw two
        cards.add(new Cards(20, "green","reverse")); //20 is reverse
        cards.add(new Cards(20, "green","reverse")); //20 is reverse
        cards.add(new Cards(50, "any","wild")); //50 is wild
        cards.add(new Cards(50, "any","4+ wild")); //50 is draw 4 wild
        cards.add(new Cards(0, "yellow","no action"));
        cards.add(new Cards(1, "yellow","no action"));
        cards.add(new Cards(1, "yellow","no action"));
        cards.add(new Cards(2, "yellow","no action"));
        cards.add(new Cards(2, "yellow","no action"));
        cards.add(new Cards(3, "yellow","no action"));
        cards.add(new Cards(3, "yellow","no action"));
        cards.add(new Cards(4, "yellow","no action"));
        cards.add(new Cards(4, "yellow","no action"));
        cards.add(new Cards(5, "yellow","no action"));
        cards.add(new Cards(5, "yellow","no action"));
        cards.add(new Cards(6, "yellow","no action"));
        cards.add(new Cards(6, "yellow","no action"));
        cards.add(new Cards(7, "yellow","no action"));
        cards.add(new Cards(7, "yellow","no action"));
        cards.add(new Cards(8, "yellow","no action"));
        cards.add(new Cards(8, "yellow","no action"));
        cards.add(new Cards(9, "yellow","no action"));
        cards.add(new Cards(9, "yellow","no action"));
        cards.add(new Cards(20, "yellow","skip")); //20 is skip
        cards.add(new Cards(20, "yellow","skip")); //20 is skip
        cards.add(new Cards(20, "yellow","2+")); //20 is draw two
        cards.add(new Cards(20, "yellow","2+")); //20 is draw two
        cards.add(new Cards(20, "yellow","reverse")); //20 is reverse
        cards.add(new Cards(20, "yellow","reverse")); //20 is reverse
        cards.add(new Cards(50, "any","wild")); //50 is wild
        cards.add(new Cards(50, "any","4+ wild")); //50 is draw 4 wild
    }
    public void shuffleDeck(){
        Collections.shuffle(cards);
        Collections.shuffle(cards);
        Collections.shuffle(cards);
    }
    public Cards getFistCardofDeck(){
        return cards.get(cards.size() - 1);
    }
    public void removeFistCardofDeck(){
        cards.remove(cards.size() - 1);
    }
}
