
public class Cards {
	private int cardNum;
	private int cardPoint;
	private String cardColor;
	private String cardAction;

	public Cards(int cn, String cc, String ca) {
		cardNum = cn;
		cardColor = cc;
		cardAction = ca;
	}

	
	public int getCardNum() {
		return cardNum;
	}

	public String getCardColor() {
		return cardColor;
	}

	public String getCardAction() {
		return cardAction;
	}

	public int getCardPoint() {
		cardPoint = cardNum;
		return cardPoint;
	}

	public String toString() {

		if(cardAction=="no action")
		return ( cardNum + " - " + cardColor );
		else
			return ( cardAction + " - " + cardColor );
	}

}
